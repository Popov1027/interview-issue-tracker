import { Route, Routes, useNavigate, Outlet } from 'react-router-dom';
import SignIn from './pages/SignIn/SignIn';
import { ToastContainer } from 'react-toastify';
import SignUp from './pages/SignUp/SignUp';
import { useEffect, useState } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import HomePage from './pages/HomePage/HomePage';
import SettingsPage from './pages/SettingsPage/SettingsPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
import ChangeThemeContainer from './components/DarkModeComponents/ChangeThemeContainer';
import { BehaviorSubject } from 'rxjs';
import NavBarOutlet from './components/NavBar/NavBarOutlet';
import ProductPage from './pages/ProductPage/ProductPage';
import { useQuery } from 'react-query';
import { UserDto } from './api-models/Api';
import { getUserProfile } from './services/UserService/userService';
import { UserDataContext } from './context/UserDataContext';
import ShoppingCartPage from './pages/ShoppingCartPage/ShoppingCartPage';
import PaymentPage from './pages/PaymentPage/PaymentPage';
import BrandPage from './pages/BrandPage/BrandPage';
import UsersPage from './pages/UsersPage/UsersPage';

function App() {
	const navigate = useNavigate();
	const accessToken: string | null = localStorage.getItem('access_token');
	const { data: userProfile } = useQuery<UserDto, Error>('userProfile', getUserProfile);
	const [newThemeSubject, setNewThemeSubject] = useState<BehaviorSubject<string>>();

	useEffect(() => {
		if (!accessToken && window.location.pathname !== '/sign-up') {
			navigate('/sign-in');
		}
	}, [navigate, accessToken]);

	const toggleTheme = () => {
		const newTheme = newThemeSubject?.getValue() === 'light' ? 'dark' : 'light';
		newThemeSubject?.next(newTheme);
	};

	return (
		<UserDataContext.Provider value={{ role: userProfile?.role }}>
			<ChangeThemeContainer setNewThemeSubject={setNewThemeSubject} />
			<ToastContainer />
			<Routes>
				<Route path="/" element={<Outlet />}>
					<Route path="/" element={<NavBarOutlet />}>
						<Route path={'/'} element={<HomePage />} />
						<Route path={'/product/:id'} element={<ProductPage />} />
						<Route path={'/settings'} element={<SettingsPage toggleTheme={toggleTheme} />} />
						<Route path={'/brand/:id'} element={<BrandPage/>} />
						<Route path={'/shopping-cart'} element={<ShoppingCartPage />} />
						<Route path={'/users'} element={<UsersPage />} />
					</Route>
					<Route path="/" element={<Outlet />}>
						<Route path={'/sign-up'} element={<SignUp />} />
						<Route path={'/sign-in'} element={<SignIn />} />
						<Route path={'/payment'} element={<PaymentPage />} />
						<Route path="*" element={<NotFoundPage />} />
					</Route>
				</Route>
			</Routes>
		</UserDataContext.Provider>
	);
}

export default App;
