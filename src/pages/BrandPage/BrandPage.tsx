import React from 'react';
import Brand from '../../components/BrandComponents/Brand';
import {useNavigate, useParams} from 'react-router-dom';
import {useMutation, useQuery} from 'react-query';
import {BrandDto} from '../../api-models/Api';
import {deleteBrand, getBrandById} from '../../services/BrandService/brandService';
import {toast} from 'react-toastify';

const BrandPage = () => {
	const {id} = useParams();
	const navigate = useNavigate();

	const {data: brand, isLoading} = useQuery<BrandDto, Error>(['brand', id], () =>
		getBrandById(id)
	);

	const {mutate: deleteMutation} = useMutation<void, Error, string | undefined>(
		() => deleteBrand(id),
		{
			onSuccess: async () => {
				toast.success('Brand has been deleted');
				navigate('/');
			}
		}
	);

	return (
		<>
			{!isLoading &&
				<Brand
					deleteMutation={deleteMutation}
					brand={brand}
				/>
			}
		</>
	);
};

export default BrandPage;