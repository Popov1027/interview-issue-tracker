import React, { FC } from 'react';
import Settings from '../../components/SettingsPageComponents/Settings';
import { useQuery } from 'react-query';
import { UserDto } from '../../api-models/Api';
import { getUserProfile } from '../../services/UserService/userService';
import EditProfileModal from '../../components/SettingsPageComponents/EditProfileModal';

interface SettingsPageProps {
  toggleTheme(): void;
}

const SettingsPage: FC<SettingsPageProps> = ({ toggleTheme }) => {
	const { data: userProfile, isLoading } = useQuery<UserDto, Error>('user', getUserProfile);

	return (
		<>
			{!isLoading && <Settings userProfile={userProfile} toggleTheme={toggleTheme} />}
			<EditProfileModal userProfile={userProfile} />
		</>
	);
};

export default SettingsPage;
