import React from 'react';
import {useMutation, useQuery} from 'react-query';
import {UserDto} from '../../api-models/Api';
import {deleteUser, getUsers} from '../../services/UserService/userService';
import UsersTable from '../../components/UsersComponents/UsersTable';
import {toast} from 'react-toastify';
import {queryClient} from '../../index';

const UsersPage = () => {
	const {data: users, isLoading} = useQuery<UserDto[], Error>('users', getUsers);

	const {mutate: deleteMutation} = useMutation<void, Error, string | undefined>(
		(userId: string | undefined) => deleteUser(userId),
		{
			onSuccess: async () => {
				toast.success('User has been deleted');
				await queryClient.invalidateQueries('users');
			}
		}
	);

	return (
		<>
			{
				!isLoading &&
                <UsersTable
                	users={users}
                	deleteMutation={deleteMutation}
                />
			}
		</>
	);
};

export default UsersPage;