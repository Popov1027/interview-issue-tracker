import React from 'react';
import Product from '../../components/ProductPageComponents/Product';
import { useMutation, useQuery } from 'react-query';
import { ProductDto } from '../../api-models/Api';
import { getProduct, deleteProduct } from '../../services/ProductService/productService';
import { useNavigate, useParams } from 'react-router-dom';
import EditProductModal from '../../components/ProductPageComponents/EditProductModal';
import { useModalStore } from '../../store/useModalStore';
import { toast } from 'react-toastify';
import { useConfirmModal } from '../../store/useConfirmModal';
import ConfirmDeletionModal from '../../components/ProductPageComponents/ConfirmDeletionModal';

const ProductPage = () => {
	const { id } = useParams();
	const navigate = useNavigate();
	const { isOpenModal } = useModalStore();
	const { isOpenConfirmModal } = useConfirmModal();
	const { data: product, isLoading } = useQuery<ProductDto, Error>(['product', id], () =>
		getProduct(id)
	);

	const { mutate: deleteMutation } = useMutation<void, Error, string | undefined>(
		() => deleteProduct(id),
		{
			onSuccess: () => {
				navigate('/');
				toast.success('Product has been deleted');
			}
		}
	);

	return (
		<>
			{!isLoading && (
				<>
					<Product product={product} />
					{isOpenConfirmModal ? <ConfirmDeletionModal deleteMutation={deleteMutation} /> : null}
					{isOpenModal ? <EditProductModal product={product} /> : null}
				</>
			)}
		</>
	);
};

export default ProductPage;
