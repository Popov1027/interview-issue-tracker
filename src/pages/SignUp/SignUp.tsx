import React from 'react';
import { useMutation } from 'react-query';
import { CustomError, RegisterDto } from '../../api-models/Api';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { registerSchema } from '../../hooks/registerSchema';
import { register } from '../../services/AuthService/authService';
import SignUpForm from '../../components/AuthComponents/SignUpForm';

const SignUp = () => {
	const navigate = useNavigate();

	const { mutate: registerUser } = useMutation((userData: RegisterDto) => register(userData), {
		onSuccess: ({ accessToken }) => {
			localStorage.setItem('access_token', accessToken);
			navigate('/');
		},
		onError: (error: CustomError) => {
			if (error.response.status === 400) {
				toast.error('Invalid credentials');
			}
		}
	});

	const useRegisterSchema = registerSchema();

	const methods = useForm<RegisterDto>({
		resolver: zodResolver(useRegisterSchema)
	});

	const { handleSubmit } = methods;

	return <SignUpForm registerUser={registerUser} handleSubmit={handleSubmit} methods={methods} />;
};

export default SignUp;
