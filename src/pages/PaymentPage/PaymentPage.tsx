import React, {useEffect} from 'react';
import {Elements} from '@stripe/react-stripe-js';
import CheckoutForm from '../../components/PaymentPageComponents/CheckoutForm';
import stripePromise from '../../stripe';
import {useOrderStore} from '../../store/useOrderStore';
import {useNavigate} from 'react-router-dom';

const PaymentPage = () => {
	const {isOrder} = useOrderStore();
	const totalPrice = localStorage.getItem('total_price');
	const navigate = useNavigate();

	useEffect(() => {
		if (!isOrder || !totalPrice) {
			navigate('/');
		}
 	}, [isOrder, totalPrice]);

	return (
		<Elements stripe={stripePromise} >
			<CheckoutForm/>
		</Elements>
	);
};

export default PaymentPage;