import React from 'react';
import { useMutation } from 'react-query';
import { CustomError, LoginDto } from '../../api-models/Api';
import { login } from '../../services/AuthService/authService';
import { useForm } from 'react-hook-form';
import { loginSchema } from '../../hooks/loginSchema';
import { zodResolver } from '@hookform/resolvers/zod';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import SignInForm from '../../components/AuthComponents/SignInForm';

const SignIn = () => {
	const navigate = useNavigate();

	const { mutate: loginUser } = useMutation((userData: LoginDto) => login(userData), {
		onSuccess: ({ accessToken }) => {
			localStorage.setItem('access_token', accessToken);
			navigate('/');
		},
		onError: (error: CustomError) => {
			if (error.response.status === 400) {
				toast.error('Invalid credentials');
			}
		}
	});

	const useLoginSchema = loginSchema();

	const methods = useForm<LoginDto>({
		resolver: zodResolver(useLoginSchema)
	});

	const { handleSubmit } = methods;

	return <SignInForm loginUser={loginUser} handleSubmit={handleSubmit} methods={methods} />;
};

export default SignIn;
