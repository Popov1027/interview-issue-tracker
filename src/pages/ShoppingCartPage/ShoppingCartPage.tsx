import React, { useState, useEffect } from 'react';
import Cart from '../../components/ShoppingCartComponents/Cart';
import { ProductDto } from '../../api-models/Api';
import { useTranslation } from 'react-i18next';
import NoItems from '../../utilities/NoItems';

const ShoppingCartPage = () => {
	const { t } = useTranslation();

	const [shoppingCart, setShoppingCart] = useState<ProductDto[]>(
		JSON.parse(localStorage.getItem('shopping_cart') || '[]')
	);
	const [quantities, setQuantities] = useState<{ [key: string]: number }>({});

	useEffect(() => {
		const defaultQuantities: { [key: string]: number } = {};
		const tempQuantities: { [key: string]: number } = {};

		shoppingCart.forEach((item) => {
			defaultQuantities[item.id] = 1;
			tempQuantities[item.id] = item.quantity || 1;
		});

		setQuantities(tempQuantities);
	}, [shoppingCart]);

	const updateLocalStorage = (updatedCart: ProductDto[]) => {
		localStorage.setItem('shopping_cart', JSON.stringify(updatedCart));
		setShoppingCart(updatedCart);
	};

	const handleDeleteProduct = (id: string) => {
		const updatedShoppingCart = shoppingCart.filter((item) => item.id !== id);
		updateLocalStorage(updatedShoppingCart);
	};

	const handleQuantityChange = (id: string, quantity: number) => {
		const updatedCart = shoppingCart.map((item) => (item.id === id ? { ...item, quantity } : item));
		updateLocalStorage(updatedCart);
	};

	const updateQuantity = (id: string, newQuantity: number) => {
		const updatedQuantity = newQuantity > 0 ? newQuantity : 1;
		setQuantities({ ...quantities, [id]: updatedQuantity });
		handleQuantityChange(id, updatedQuantity);
	};

	const increaseQuantity = (id: string) => {
		const updatedQuantity = (quantities[id] || 0) + 1;
		updateQuantity(id, updatedQuantity);
	};

	const decreaseQuantity = (id: string) => {
		const updatedQuantity = (quantities[id] || 0) - 1;
		updateQuantity(id, updatedQuantity);
	};

	const totalPrice = shoppingCart.reduce(
		(accumulator, currentItem) =>
			accumulator + (currentItem.price || 0) * (quantities[currentItem.id] || 1),
		0
	);

	return (
		<>
			{shoppingCart.length !== 0 ? (
				<Cart
					quantities={quantities}
					handleQuantityChange={handleQuantityChange}
					increaseQuantity={increaseQuantity}
					decreaseQuantity={decreaseQuantity}
					totalPrice={totalPrice}
					shoppingCart={shoppingCart}
					handleDeleteProduct={handleDeleteProduct}
				/>
			) : (
				<NoItems>{t('shoppingCart.noProducts')}</NoItems>
			)}
		</>
	);
};

export default ShoppingCartPage;
