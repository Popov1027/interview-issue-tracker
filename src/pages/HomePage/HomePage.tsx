import React, {useContext, useState} from 'react';
import {useQuery} from 'react-query';
import {BrandDto, ProductDto, UpdatedProductDto} from '../../api-models/Api';
import {getProducts} from '../../services/ProductService/productService';
import ProductsList from '../../components/HomePageComponents/ProductsList';
import SearchBar from '../../components/HomePageComponents/SearchBar';
import SortArrows from '../../components/HomePageComponents/SortArrows';
import CreateProductButton from '../../components/HomePageComponents/CreateProductsComponents/CreateProductButton';
import CreateProductModal from '../../components/HomePageComponents/CreateProductsComponents/CreateProductModal';
import {UserDataContext, UserDataContextInterface} from '../../context/UserDataContext';
import {toast} from 'react-toastify';
import {useTranslation} from 'react-i18next';
import NoItems from '../../utilities/NoItems';
import CategoryList from '../../components/BrandComponents/BrandList';
import {getBrands} from '../../services/BrandService/brandService';
import CreateBrandButton from '../../components/BrandComponents/CreateBrandButton';
import CreateBrandModal from '../../components/BrandComponents/CreateBrandModal';
import ExportFileButton from '../../utilities/ExportFileButton';

const HomePage = () => {
	const {t} = useTranslation();
	const {role} = useContext<UserDataContextInterface>(UserDataContext);

	const [shoppingCart, setShoppingCart] = useState<ProductDto[]>(JSON.parse(localStorage.getItem('shopping_cart') || '[]'));
	const [searchInput, setSearchInput] = useState<string>('');
	const [sortProducts, setSortProducts] = useState<'asc' | 'desc'>('asc');
	const [selectedCategory, setSelectedCategory] = useState<number | null>(null);

	const {data: products, isLoading} = useQuery<ProductDto[], Error>('products', getProducts);
	const {data: brands } = useQuery<BrandDto[], Error>('brands', getBrands);

	const searchProducts = products?.filter((product) => {
		if (product.name.toLowerCase().includes(searchInput.toLowerCase())) {
			return product;
		}
		return null;
	});

	const sortedProducts = searchProducts?.slice().sort((a, b) => {
		if (sortProducts === 'asc') {
			return a.price - b.price;
		} else {
			return b.price - a.price;
		}
	});

	const filteredProducts = selectedCategory !== null
		? products?.filter((product) => product.brandId === selectedCategory)
		: sortedProducts;

	const updatedFilteredProducts: UpdatedProductDto[] | undefined = filteredProducts?.map(product => ({
		...product,
		brand: product.brand?.name
	}));

	const handleAddShoppingCart = (product: ProductDto) => {
		const updatedShoppingCart = [...shoppingCart];

		const productIndex = updatedShoppingCart.findIndex(
			(favTask: ProductDto) => favTask.id === product.id
		);

		if (productIndex === -1) {
			toast.success(t('shoppingCart.addMessage'));
			updatedShoppingCart.push(product);
		} else {
			updatedShoppingCart.splice(productIndex, 1);
		}

		setShoppingCart(updatedShoppingCart);
		localStorage.setItem('shopping_cart', JSON.stringify(updatedShoppingCart));
	};

	return (
		<div className="mt-8 tablet:mx-24 mobileL:mx-10 mx-2 space-y-8">
			<SearchBar searchInput={searchInput} setSearchInput={setSearchInput}/>
			<div className="flex justify-center tablet:justify-end items-center space-x-3">
				<SortArrows sortProducts={sortProducts} setSortProducts={setSortProducts}/>
				{role === 'ADMIN' ?
					<div className="flex flex-col tablet:flex-row m-0 space-y-2 tablet:space-y-0 tablet:space-x-2">
    					<CreateProductButton/>
	                    <CreateBrandButton/>
						<ExportFileButton data={updatedFilteredProducts}/>
					</div>
					: null}
				<CreateProductModal brands={brands}/>
				<CreateBrandModal/>
			</div>
			{!isLoading || filteredProducts?.length !== 0 ? (
				<div className="flex flex-col space-x-0 laptop:flex-row laptop:space-x-8">
					<CategoryList
						selectedCategory={selectedCategory}
						setSelectedCategory={setSelectedCategory}
						brands={brands}
					/>
					<ProductsList
						products={filteredProducts}
						handleAddShoppingCart={handleAddShoppingCart}
						shoppingCart={shoppingCart}
					/>
				</div>
			) : (
				<NoItems>{t('shoppingCart.noProducts')}</NoItems>
			)}
		</div>
	);
};

export default HomePage;
