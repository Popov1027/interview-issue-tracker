import { LoginDto, RegisterDto, TokensDto } from '../../api-models/Api';
import http from '../http/http';

const register = async (inputData: RegisterDto): Promise<TokensDto> => {
	const response = await http.post<TokensDto>('register', inputData);
	return response.data;
};

const login = async (inputData: LoginDto): Promise<TokensDto> => {
	const response = await http.post<TokensDto>('login', inputData);
	return response.data;
};

export { register, login };
