import { UpdateUserDto, UserDto } from '../../api-models/Api';
import http from '../http/http';

const getUserProfile = async (): Promise<UserDto> => {
	const response = await http.get<UserDto>('user/profile');
	return response.data;
};

const getUsers = async (): Promise<UserDto[]> => {
	const response = await http.get<UserDto[]>('user');
	return response.data;
};

const deleteUser = async (userId: string | undefined): Promise<void> => {
	const response = await http.delete<void>(`user/${userId}`);
	return response.data;
};


const updateUserProfile = async (inputData: UpdateUserDto): Promise<UserDto> => {
	const response = await http.patch<UserDto>('user/profile/me', inputData);
	return response.data;
};

export {
	getUserProfile,
	updateUserProfile,
	getUsers,
	deleteUser
};
