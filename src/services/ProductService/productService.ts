import { CreateProductDto, ProductDto, UpdateProductDto } from '../../api-models/Api';
import http from '../http/http';

const getProducts = async (): Promise<ProductDto[]> => {
	const response = await http.get<ProductDto[]>('product');
	return response.data;
};

const getProduct = async (id: string | undefined): Promise<ProductDto> => {
	const response = await http.get<ProductDto>(`product/${id}`);
	return response.data;
};

const createProduct = async (inputData: CreateProductDto): Promise<ProductDto> => {
	const response = await http.post<ProductDto>('product', inputData);
	return response.data;
};

const editProduct = async (
	id: string | undefined,
	inputData: UpdateProductDto
): Promise<ProductDto> => {
	const response = await http.patch<ProductDto>(`product/${id}`, inputData);
	return response.data;
};

const deleteProduct = async (id: string | undefined): Promise<void> => {
	const response = await http.delete<void>(`product/${id}`);
	return response.data;
};

export { getProducts, getProduct, createProduct, deleteProduct, editProduct };
