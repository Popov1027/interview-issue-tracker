import {BrandDto, CreateBrandDto} from '../../api-models/Api';
import http from '../http/http';

const getBrands = async (): Promise<BrandDto[]> => {
	const response = await http.get<BrandDto[]>('brand');
	return response.data;
};

const getBrandById = async (id: string | undefined): Promise<BrandDto> => {
	const response = await http.get<BrandDto>(`brand/${id}`);
	return response.data;
};

const createBrand = async (inputData: CreateBrandDto): Promise<BrandDto> => {
	const response = await http.post<BrandDto>('brand', inputData);
	return response.data;
};

const editBrand = async (inputData: CreateBrandDto, brandId: string | undefined): Promise<BrandDto> => {
	const response = await http.patch<BrandDto>(`brand/${brandId}`, inputData);
	return response.data;
};

const deleteBrand = async (brandId: string | undefined): Promise<void> => {
	const response = await http.delete<void>(`brand/${brandId}`);
	return response.data;
};

export {
	getBrands,
	getBrandById,
	createBrand,
	deleteBrand,
	editBrand
};
