import axios, { AxiosResponse, InternalAxiosRequestConfig } from 'axios';

const http = axios.create({
	baseURL: 'https://test-backend-bm5r.onrender.com/api/'
});

http.interceptors.request.use((config: InternalAxiosRequestConfig) => {
	const token = localStorage.getItem('access_token');
	const customConfig: InternalAxiosRequestConfig = { ...config };
	customConfig.headers.Authorization = token ? `Bearer ${token}` : '';
	return customConfig;
});

http.interceptors.response.use(
	(response: AxiosResponse) => {
		return response;
	},
	(error) => {
		if (error.response && error.response.status === 401) {
			localStorage.removeItem('access_token');
		}
		return Promise.reject(error);
	}
);

export default {
	get: http.get,
	post: http.post,
	patch: http.patch,
	delete: http.delete
};
