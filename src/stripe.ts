import { loadStripe } from '@stripe/stripe-js';

const stripePromise = loadStripe('pk_test_51OKKRaE60CFaRuFBLXTUlthPQqy0WdAhYE7jILuGHb784dBVmN6pvPvRGkoCpfV8UF9k9uFZcYCQDfRvGWjLwpkO00q4IiBbeU');

export default stripePromise;