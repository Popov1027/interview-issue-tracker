import {useTranslation} from 'react-i18next';

interface LinksInterface {
    path: string;
    title: string;
}

export const useNavBarLinks = ():LinksInterface[] => {
	const { t } = useTranslation();

	return [
		{path: '/settings', title: t('navBar.settings')},
		{path: '/shopping-cart', title: t('navBar.shoppingCart')}
	];
};