import XLSX from 'sheetjs-style';
import FileSaver from 'file-saver';
import {UpdatedProductDto, UserDto} from '../api-models/Api';

export const exportToExcel = (data: UserDto[] | UpdatedProductDto[] | undefined, bookType: 'xlsx' | 'csv') => {
	if (!data) {
		return;
	}
	const ws = XLSX.utils.json_to_sheet(data);
	const wb = {Sheets: {'data': ws}, SheetNames: ['data']};
	const excelBuffer = XLSX.write(wb, {bookType: bookType, type: 'array'});
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
	FileSaver.saveAs(new Blob([excelBuffer], {type: fileType}), 'file.' + bookType
	);
};