import { object, string } from 'zod';
import { useTranslation } from 'react-i18next';

export const loginSchema = () => {
	const { t } = useTranslation();

	return object({
		email: string().min(1, t('loginSchema.email.required')).email(t('loginSchema.email.invalid')),
		password: string()
			.min(1, t('loginSchema.password.required'))
			.min(8, t('loginSchema.password.minLength'))
			.max(32, t('loginSchema.password.maxLength'))
	});
};
