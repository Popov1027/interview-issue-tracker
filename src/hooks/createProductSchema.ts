import { number, object, string } from 'zod';
import { useTranslation } from 'react-i18next';

export const useCreateProductSchema = () => {
	const { t } = useTranslation();

	return object({
		name: string().min(1, t('createProductSchema.name')),
		description: string().min(1, t('createProductSchema.description')),
		currency: string().min(1, t('createProductSchema.currency')),
		price: number().min(1, t('createProductSchema.price')),
		brandId: number().min(1, t('createProductSchema.brand'))
	});
};
