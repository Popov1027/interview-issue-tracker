import { object, string } from 'zod';
import { useTranslation } from 'react-i18next';

export const useCreateBrandSchema = () => {
	const { t } = useTranslation();

	return object({
		name: string().min(1, t('createBrandSchema.name')),
	});
};
