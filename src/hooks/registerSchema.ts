import { object, string } from 'zod';
import { useTranslation } from 'react-i18next';

export const registerSchema = () => {
	const { t } = useTranslation();

	return object({
		email: string()
			.min(1, t('registerSchema.email.required'))
			.email(t('registerSchema.email.invalid')),
		firstName: string().min(1, t('registerSchema.firstName.required')).max(100),
		lastName: string().min(1, t('registerSchema.lastName.required')).max(100),
		phoneNumber: string().min(1, t('registerSchema.phoneNumber.required')).max(20),
		password: string()
			.min(1, t('registerSchema.password.required'))
			.min(8, t('registerSchema.password.minLength'))
			.max(32, t('registerSchema.password.maxLength'))
	});
};
