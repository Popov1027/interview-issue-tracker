import { object, string } from 'zod';
import { useTranslation } from 'react-i18next';

export const useUserProfileSchema = () => {
	const { t } = useTranslation();

	return object({
		firstName: string().min(1, t('userInputsValidation.firstName')),
		lastName: string().min(1, t('userInputsValidation.lastName')),
		email: string().min(1, t('userInputsValidation.email')),
		phoneNumber: string().min(1, t('userInputsValidation.phoneNumber')),
		gender: string().min(1, t('userInputsValidation.gender'))
	});
};
