import React, { HTMLInputTypeAttribute } from 'react';
import { useFormContext } from 'react-hook-form';

interface FormInputProps {
  label: string;
  name: string;
  type?: HTMLInputTypeAttribute;
  defaultValue?: string | number | null | undefined;
}

const Input: React.FC<FormInputProps> = ({ label, name, type, defaultValue }) => {
	const {
		register,
		formState: { errors }
	} = useFormContext();

	return (
		<div className="block w-full">
			<label htmlFor={name} className="block mb-3">
				{label}
			</label>
			<input
				type={type}
				placeholder=" "
				defaultValue={defaultValue ?? ''}
				className="block border border-gray-300 dark:border-gray-600 dark:bg-[#1f2937] w-full rounded-2xl appearance-none focus:outline-none py-2 px-4"
				{...register(name, name === 'price' ? { valueAsNumber: true } : {})}
			/>
			{errors[name] && (
				<span className="text-red-500 text-xs pt-1 block">{errors[name]?.message as string}</span>
			)}
		</div>
	);
};

export default Input;
