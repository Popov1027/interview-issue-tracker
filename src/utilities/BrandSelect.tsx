import React from 'react';
import { useFormContext } from 'react-hook-form';
import { BrandDto } from '../api-models/Api';

interface FormInputProps {
  label: string;
  name: string;
  value: BrandDto[] | undefined;
}

const BrandSelect: React.FC<FormInputProps> = ({ label, name, value }) => {
	const {
		register,
		formState: { errors }
	} = useFormContext();
	return (
		<div className="block w-full">
			<label htmlFor={name} className="block mb-3">
				{label}
			</label>
			<select
				className="block border border-gray-300 dark:border-gray-600 dark:bg-[#1f2937] w-full rounded-2xl appearance-none focus:outline-none py-2 px-4"
				{...register(name, { valueAsNumber: true })}
			>
				{value?.map((brand) => (
					<option key={brand.id} value={brand.id}>
						{brand.name}
					</option>
				))}
			</select>
			{errors[name] && (
				<span className="text-red-500 text-xs pt-1 block">{errors[name]?.message as string}</span>
			)}
		</div>
	);
};

export default BrandSelect;
