import React, { FC, PropsWithChildren } from 'react';

const NoItems: FC<PropsWithChildren> = ({ children }) => {
	return (
		<div className="mt-28 flex justify-center items-center">
			<p className="text-4xl font-bold rounded-full border border-gray-600 px-12 py-6">
				{children}
			</p>
		</div>
	);
};

export default NoItems;
