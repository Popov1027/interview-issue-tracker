import React, { FC, FormEvent } from 'react';
import { FormProvider, UseFormHandleSubmit, UseFormReturn } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useModalStore } from '../store/useModalStore';
import Input from './Input';
import BrandSelect from './BrandSelect';
import { BrandDto, CreateProductDto, CustomError, ProductDto } from '../api-models/Api';
import { UseMutateFunction } from 'react-query';

interface ProductFormInterface {
  methods: UseFormReturn<CreateProductDto>;
  brands: BrandDto[] | undefined;
  handleSubmit: UseFormHandleSubmit<CreateProductDto>;
  newProduct: UseMutateFunction<ProductDto, CustomError, CreateProductDto>;
  product?: ProductDto | undefined;
}

const ProductForm: FC<ProductFormInterface> = ({
	brands,
	handleSubmit,
	methods,
	newProduct,
	product
}) => {
	const { setIsOpenModal } = useModalStore();
	const { t } = useTranslation();

	return (
		<div
			onClick={() => setIsOpenModal(false)}
			className="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 flex justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full backdrop-blur-sm"
		>
			<div
				onClick={(e: FormEvent) => e.stopPropagation()}
				className="relative p-4 w-full max-w-3xl max-h-full"
			>
				<div className="relative bg-white rounded-lg shadow dark:bg-gray-700 w-full">
					<div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
						<h3 className="text-xl font-semibold text-gray-900 dark:text-white">
							{!product ? t('homePage.createProduct') : t('homePage.editProduct')}
						</h3>
						<button
							onClick={() => setIsOpenModal(false)}
							type="button"
							className="end-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
							data-modal-hide="authentication-modal"
						>
							<svg
								className="w-3 h-3"
								aria-hidden="true"
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 14 14"
							>
								<path
									stroke="currentColor"
									strokeLinecap="round"
									strokeLinejoin="round"
									strokeWidth="2"
									d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
								/>
							</svg>
							<span className="sr-only">Close modal</span>
						</button>
					</div>
					<div className="p-4 md:p-5">
						<FormProvider {...methods}>
							<form
								onSubmit={handleSubmit((values) => {
									newProduct(values);
								})}
								className="space-y-4"
							>
								<Input
									label={t('homePage.name')}
									name="name"
									type="text"
									defaultValue={product?.name || ''}
								/>
								<Input
									label={t('homePage.description')}
									name="description"
									type="text"
									defaultValue={product?.description || ''}
								/>
								<Input
									label={t('homePage.currency')}
									name="currency"
									type="text"
									defaultValue={product?.currency || ''}
								/>
								<div className="flex justify-between w-full space-x-4">
									<Input
										label={t('homePage.price')}
										name="price"
										type="number"
										defaultValue={product?.price || ''}
									/>
									<BrandSelect label={t('homePage.brand')} name="brandId" value={brands} />
								</div>
								<div className="flex justify-center">
									<button className="bg-ct-blue-600 hover:bg-yellow-500 transition py-2 px-3 rounded-full text-white font-bold">
										{t('createProductSchema.submit')}
									</button>
								</div>
							</form>
						</FormProvider>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ProductForm;
