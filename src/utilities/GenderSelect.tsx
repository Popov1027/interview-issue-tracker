import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Gender } from '../api-models/UserEnums';

interface FormInputProps {
  label: string;
  name: string;
}

const GenderSelect: React.FC<FormInputProps> = ({ label, name }) => {
	const {
		register,
		formState: { errors }
	} = useFormContext();

	return (
		<div className="block w-full">
			<label htmlFor={name} className="block mb-3">
				{label}
			</label>
			<select
				className="block border border-gray-300 dark:border-gray-600 dark:bg-[#1f2937] w-full rounded-2xl appearance-none focus:outline-none py-2 px-4"
				{...register(name)}
			>
				{Object.values(Gender).map((gender) => (
					<option key={gender} value={gender}>
						{gender}
					</option>
				))}
			</select>
			{errors[name] && (
				<span className="text-red-500 text-xs pt-1 block">{errors[name]?.message as string}</span>
			)}
		</div>
	);
};

export default GenderSelect;
