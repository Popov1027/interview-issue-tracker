import React, {FC} from 'react';
import {FaFileDownload} from 'react-icons/fa';
import {BookType} from '../api-models/UserEnums';
import {exportToExcel} from '../hooks/exportToExcel';
import {useOpenBookType} from '../store/useOpenBookType';
import {useBookStore} from '../store/useBookType';
import {UpdatedProductDto, UserDto} from '../api-models/Api';

interface ExportFileButtonInterface {
	data: UserDto[] |  UpdatedProductDto[] | undefined
}

const ExportFileButton:FC<ExportFileButtonInterface> = ({data}) => {
	const {setIsOpenBookType, isOpenBookType} = useOpenBookType();
	const {setBookType, bookType} = useBookStore();

	return (
		<div
			onClick={() => setIsOpenBookType(!isOpenBookType)}
			className="relative flex items-center justify-center space-x-2 w-44 cursor-pointer transition duration-300 rounded-full font-bold border py-1 px-3 h-10"
		>
			<FaFileDownload/>
			<p>Export file</p>
			{isOpenBookType &&
                <div className="z-10 absolute bottom-[-80px] right-12 bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700">
                	{Object.values(BookType).map((type, index) => (
                		<ul
                			key={index}
                			className="text-sm text-gray-700 dark:text-gray-200 w-16">
                			<li
                				onClick={() => {
                					setBookType(type);
                					exportToExcel(data, bookType);
                					setIsOpenBookType(!isOpenBookType);
                				}}
                				className="block px-4 py-2 hover:text-blue-500 transition">
                				{type}
                			</li>
                		</ul>
                	))}
                </div>
			}
		</div>
	);
};

export default ExportFileButton;