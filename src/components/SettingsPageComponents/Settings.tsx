import React, { FC } from 'react';
import DarkModeToggle from '../DarkModeComponents/DarkModeToggle';
import TranslationButton from '../TranslationComponents/TranslationButton';
import { UserDto } from '../../api-models/Api';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { useModalStore } from '../../store/useModalStore';
import {MdEmail} from 'react-icons/md';
import {FaPhoneAlt} from 'react-icons/fa';

interface SettingsProps {
  toggleTheme(): void;
  userProfile: UserDto | undefined;
}

const Settings: FC<SettingsProps> = ({ toggleTheme, userProfile }) => {
	const { t } = useTranslation();
	const { setIsOpenModal } = useModalStore();

	return (
		<div className="flex flex-col justify-center items-center max-w-5xl w-full mx-auto overflow-hidden shadow-xl rounded-2xl p-12 space-y-8 h-full">
			<div className="flex flex-col items-center space-y-4 border-b w-1/2 pb-4">
				<div className="w-40 h-40 bg-gray-500 rounded-full text-white flex justify-center items-center text-6xl uppercase">
					<p>{userProfile?.firstName?.charAt(0)}</p>
					<p>{userProfile?.lastName?.charAt(0)}</p>
				</div>
				<div className="flex flex-col items-center">
					<p className="text-gray-500">
						{t('userProfile.joinedAt')} {moment(userProfile?.createdAt).format('LL')}
					</p>
				</div>
			</div>
			<div className="text-xl flex flex-col items-center laptop:flex-row justify-evenly w-full space-y-2">
				<p className="font-bold">
					<span className="font-light text-gray-400">{t('userProfile.firstName')} </span>
					{userProfile?.firstName}
				</p>
				<p className="font-bold">
					<span className="font-light text-gray-400">{t('userProfile.lastName')} </span>
					{userProfile?.lastName}
				</p>
			</div>
			<div className="text-xl flex flex-col items-center laptop:flex-row justify-evenly w-full space-y-2">
				<div className="font-bold flex flex-row space-x-2 items-end">
					<MdEmail className="tablet:hidden flex text-2xl"/>
					<span className="font-light text-gray-400 hidden tablet:flex">{t('userProfile.email')} </span>
					<span>{userProfile?.email}</span>
				</div>
				<div className="font-bold flex flex-row space-x-2 items-center">
					<FaPhoneAlt className="tablet:hidden flex text-xl"/>
					<span className="font-light text-gray-400 hidden tablet:flex">{t('userProfile.phoneNumber')} </span>
					<span>{userProfile?.phoneNumber}</span>
				</div>
			</div>
			{(userProfile?.gender || userProfile?.role) && (
				<div className="text-xl flex flex-col tablet:flex-row justify-evenly items-center w-full">
					<p className="font-bold">
						<span className="font-light text-gray-400">{t('userProfile.gender')} </span>
						{userProfile?.gender}
					</p>
					<p className="font-bold">
						<span className="font-light text-gray-400">{t('userProfile.role')} </span>
						{userProfile?.role !== 'ADMIN' ? 'USER' : 'ADMIN'}
					</p>
				</div>
			)}
			<div className="flex flex-col tablet:flex-row justify-center space-y-4 tablet:space-y-0 space-x-0 tablet:space-x-12 mt-28 w-full">
				<DarkModeToggle toggleTheme={toggleTheme} />
				<TranslationButton />
			</div>
			<div className="flex justify-center tablet:justify-end w-full">
				<button
					onClick={() => setIsOpenModal(true)}
					className="font-bold text-center border rounded-full px-4 py-2 hover:bg-blue-500 transition hover:text-white"
				>
					{t('userProfile.edit')}
				</button>
			</div>
		</div>
	);
};

export default Settings;
