import React, { FC } from 'react';
import { FormProvider, UseFormHandleSubmit, UseFormReturn } from 'react-hook-form';
import Input from '../../utilities/Input';
import { useTranslation } from 'react-i18next';
import { useModalStore } from '../../store/useModalStore';
import { UseMutateFunction } from 'react-query';
import { CustomError, UpdateUserDto, UserDto } from '../../api-models/Api';
import GenderSelect from '../../utilities/GenderSelect';

interface ProfileModalFormInterface {
  newUserData: UseMutateFunction<UserDto, CustomError, UpdateUserDto, unknown>;
  handleSubmit: UseFormHandleSubmit<UpdateUserDto, undefined>;
  userProfile: UserDto | undefined;
  methods: UseFormReturn<UpdateUserDto>;
}

const ProfileModalForm: FC<ProfileModalFormInterface> = ({
	newUserData,
	handleSubmit,
	userProfile,
	methods
}) => {
	const { t } = useTranslation();
	const { setIsOpenModal } = useModalStore();

	return (
		<div className="overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 flex justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full backdrop-blur-sm">
			<div className="relative p-4 w-full max-w-3xl max-h-full">
				<div className="relative bg-white rounded-lg shadow dark:bg-gray-700 w-full">
					<div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
						<h3 className="text-xl font-semibold text-gray-900 dark:text-white">
							{t('userProfile.updateUser')}
						</h3>
						<button
							onClick={() => setIsOpenModal(false)}
							type="button"
							className="end-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
							data-modal-hide="authentication-modal"
						>
							<svg
								className="w-3 h-3"
								aria-hidden="true"
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 14 14"
							>
								<path
									stroke="currentColor"
									strokeLinecap="round"
									strokeLinejoin="round"
									strokeWidth="2"
									d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
								/>
							</svg>
							<span className="sr-only">Close modal</span>
						</button>
					</div>
					<div className="p-4 md:p-5">
						<FormProvider {...methods}>
							<form
								onSubmit={handleSubmit((values) => {
									newUserData(values);
								})}
								className="space-y-4"
							>
								<Input
									label={t('userProfile.firstName')}
									name="firstName"
									type="text"
									defaultValue={userProfile?.firstName}
								/>
								<Input
									label={t('userProfile.lastName')}
									name="lastName"
									type="text"
									defaultValue={userProfile?.lastName}
								/>
								<div className="flex space-x-4">
									<Input
										label={t('userProfile.email')}
										name="email"
										type="text"
										defaultValue={userProfile?.email}
									/>
									<Input
										label={t('userProfile.phoneNumber')}
										name="phoneNumber"
										type="text"
										defaultValue={userProfile?.phoneNumber}
									/>
								</div>
								<GenderSelect label={t('userProfile.gender')} name="gender" />
								<div className="flex justify-center">
									<button className="bg-ct-blue-600 hover:bg-yellow-500 transition py-2 px-3 rounded-full text-white font-bold">
										{t('createProductSchema.create')}
									</button>
								</div>
							</form>
						</FormProvider>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ProfileModalForm;
