import React, { FC } from 'react';
import { CustomError, UpdateUserDto, UserDto } from '../../api-models/Api';
import { useModalStore } from '../../store/useModalStore';
import { useMutation } from 'react-query';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { updateUserProfile } from '../../services/UserService/userService';
import ProfileModalForm from './ProfileModalForm';
import { useUserProfileSchema } from '../../hooks/updateUserProfileSchema';
import { queryClient } from '../../index';

interface EditProfileModal {
  userProfile: UserDto | undefined;
}

const EditProfileModal: FC<EditProfileModal> = ({ userProfile }) => {
	const { setIsOpenModal, isOpenModal } = useModalStore();

	const { mutate: newUserData } = useMutation(
		(userData: UpdateUserDto) => updateUserProfile(userData),
		{
			onSuccess: async (data: UserDto) => {
				await queryClient.invalidateQueries('user');
				localStorage.setItem('role', data.role || 'USER');
				toast.success('User has been updated');
				setIsOpenModal(false);
			},
			onError: (error: CustomError) => {
				if (error.response.status === 400) {
					toast.error(error.response.message);
				}
			}
		}
	);

	const updateUserProfileSchema = useUserProfileSchema();

	const methods = useForm<UpdateUserDto>({
		resolver: zodResolver(updateUserProfileSchema)
	});

	const { handleSubmit } = methods;

	return (
		<>
			{isOpenModal ? (
				<ProfileModalForm
					newUserData={newUserData}
					handleSubmit={handleSubmit}
					userProfile={userProfile}
					methods={methods}
				/>
			) : null}
		</>
	);
};

export default EditProfileModal;
