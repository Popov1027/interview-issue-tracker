import React, { FC } from 'react';
import { FormProvider, UseFormHandleSubmit, UseFormReturn } from 'react-hook-form';
import Input from '../../utilities/Input';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { CustomError, LoginDto, TokensDto } from '../../api-models/Api';
import { UseMutateFunction } from 'react-query';

interface SignInForm {
  loginUser: UseMutateFunction<TokensDto, CustomError, LoginDto>;
  handleSubmit: UseFormHandleSubmit<LoginDto>;
  methods: UseFormReturn<LoginDto>;
}

const SignInForm: FC<SignInForm> = ({ loginUser, methods, handleSubmit }) => {
	const { t } = useTranslation();

	return (
		<div className="flex justify-center items-center h-screen">
			<FormProvider {...methods}>
				<form
					onSubmit={handleSubmit((values) => {
						loginUser(values);
					})}
					className="flex flex-col justify-center max-w-xl w-full mx-auto overflow-hidden shadow-xl rounded-2xl p-8 space-y-5"
				>
					<div className="text-blue-800 dark:text-gray-500 text-2xl flex justify-center font-bold">
						<h1>{t('auth.signIn')}</h1>
					</div>
					<Input label={t('auth.email')} name="email" type="email" />
					<Input label={t('auth.password')} name="password" type="password" />
					<Link to={'/sign-up'} className="text-sm text-yellow-500 font-bold">
						{t('auth.createAccount')}
					</Link>
					<button className="mx-24 bg-ct-blue-600 hover:bg-yellow-500 transition py-2 rounded-full text-white font-bold">
						{t('auth.login')}
					</button>
				</form>
			</FormProvider>
		</div>
	);
};

export default SignInForm;
