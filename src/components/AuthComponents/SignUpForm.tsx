import React, { FC } from 'react';
import { FormProvider, UseFormHandleSubmit, UseFormReturn } from 'react-hook-form';
import Input from '../../utilities/Input';
import { useTranslation } from 'react-i18next';
import { CustomError, RegisterDto, TokensDto } from '../../api-models/Api';
import { UseMutateFunction } from 'react-query';

interface SignUpFormInterface {
  registerUser: UseMutateFunction<TokensDto, CustomError, RegisterDto>;
  handleSubmit: UseFormHandleSubmit<RegisterDto>;
  methods: UseFormReturn<RegisterDto>;
}

const SignUpForm: FC<SignUpFormInterface> = ({ registerUser, methods, handleSubmit }) => {
	const { t } = useTranslation();

	return (
		<div className="flex justify-center items-center h-screen">
			<FormProvider {...methods}>
				<form
					onSubmit={handleSubmit((values) => {
						registerUser(values);
					})}
					className="flex flex-col justify-center max-w-2xl w-full mx-auto overflow-hidden shadow-xl rounded-2xl p-8 space-y-5"
				>
					<div className="text-blue-800 dark:text-gray-500 text-2xl flex justify-center font-bold">
						<h1>{t('auth.signUp')}</h1>
					</div>
					<Input label={t('auth.email')} name="email" type="email" />
					<Input label={t('auth.firstName')} name="firstName" type="text" />
					<Input label={t('auth.lastName')} name="lastName" type="text" />
					<Input label={t('auth.phoneNumber')} name="phoneNumber" type="text" />
					<Input label={t('auth.password')} name="password" type="password" />
					<button className="mx-24 bg-ct-blue-600 hover:bg-yellow-500 transition py-2 rounded-full text-white font-bold">
						{t('auth.register')}
					</button>
				</form>
			</FormProvider>
		</div>
	);
};

export default SignUpForm;
