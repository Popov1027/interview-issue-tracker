import React, {FC} from 'react';
import { useModalStore } from '../../../store/useModalStore';
import { useMutation } from 'react-query';
import { BrandDto, CreateProductDto, CustomError } from '../../../api-models/Api';
import { toast } from 'react-toastify';
import { createProduct } from '../../../services/ProductService/productService';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { useCreateProductSchema } from '../../../hooks/createProductSchema';
import { queryClient } from '../../../index';
import ProductForm from '../../../utilities/ProductForm';

interface CreateProductModalInterface {
	brands: BrandDto[] | undefined
}

const CreateProductModal:FC<CreateProductModalInterface> = ({brands}) => {
	const { isOpenModal, setIsOpenModal } = useModalStore();

	const { mutate: newProduct } = useMutation(
		(productData: CreateProductDto) => createProduct(productData),
		{
			onSuccess: async () => {
				await queryClient.invalidateQueries('products');
				toast.success('Product has been created');
				setIsOpenModal(false);
			},
			onError: (error: CustomError) => {
				if (error.response.status === 400) {
					toast.error(error.response.message);
				}
			}
		}
	);

	const createProductSchema = useCreateProductSchema();

	const methods = useForm<CreateProductDto>({
		resolver: zodResolver(createProductSchema)
	});

	const { handleSubmit } = methods;

	return isOpenModal ? (
		<ProductForm
			newProduct={newProduct}
			methods={methods}
			brands={brands}
			handleSubmit={handleSubmit}
		/>
	) : null;
};

export default CreateProductModal;
