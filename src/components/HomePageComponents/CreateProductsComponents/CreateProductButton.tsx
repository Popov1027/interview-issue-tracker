import React from 'react';
import { FaPlus } from 'react-icons/fa';
import { useTranslation } from 'react-i18next';
import { useModalStore } from '../../../store/useModalStore';

const CreateProductButton = () => {
	const { t } = useTranslation();
	const { setIsOpenModal } = useModalStore();

	return (
		<div
			onClick={() => setIsOpenModal(true)}
			className="flex items-center justify-center w-44 space-x-2 cursor-pointer transition duration-300 hover:bg-gray-800 hover:text-white dark:hover:bg-white dark:hover:text-blue-950 rounded-full font-bold border py-1 px-3 h-10"
		>
			<FaPlus />
			<p>{t('homePage.createProduct')}</p>
		</div>
	);
};

export default CreateProductButton;
