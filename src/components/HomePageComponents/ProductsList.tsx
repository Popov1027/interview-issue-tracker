import React, { FC, FormEvent } from 'react';
import { ProductDto } from '../../api-models/Api';
import { useNavigate } from 'react-router-dom';
import { IoMdCart } from 'react-icons/io';

interface ProductsListInterface {
  products: ProductDto[] | undefined;
  handleAddShoppingCart(task: ProductDto): void;
  shoppingCart: ProductDto[];
}

const ProductsList: FC<ProductsListInterface> = ({
	products,
	handleAddShoppingCart,
	shoppingCart
}) => {
	const navigate = useNavigate();

	return (
		<div className="flex justify-center w-full">
			<ul className="grid w-full gap-6 md:grid-cols-3">
				{products?.map((product) => (
					<li onClick={() => navigate(`/product/${product.id}`)} key={product.id}>
						<label className="flex flex-col h-52 justify-between w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer transition duration-500 dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-blue-600 hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">
							<div className="flex justify-between">
								<div className="block">
									<div className="w-full text-lg font-semibold">{product.name}</div>
									<div className="w-full text-sm">{product.description}</div>
								</div>
								<p className="text-sm font-light">{product.brand?.name}</p>
							</div>
							<div className="flex justify-between items-center">
								<p className="text-2xl font-bold">{product.price}$</p>
								<IoMdCart
									className={`${
										shoppingCart.some((favTask: ProductDto) => favTask.id === product.id)
											? 'text-blue-500'
											: 'text-gray-500'
									} text-4xl hover:text-blue-500 transition duration-300`}
									onClick={(e: FormEvent) => {
										e.stopPropagation();
										handleAddShoppingCart(product);
									}}
								/>
							</div>
						</label>
					</li>
				))}
			</ul>
		</div>
	);
};

export default ProductsList;
