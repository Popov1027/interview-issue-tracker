import React, { FC } from 'react';
import { BiSortDown, BiSortUp } from 'react-icons/bi';

interface SortArrowsProps {
  sortProducts: 'asc' | 'desc';
  setSortProducts: React.Dispatch<React.SetStateAction<'asc' | 'desc'>>;
}

const SortArrows: FC<SortArrowsProps> = ({ setSortProducts, sortProducts }) => {
	const handleSortToggle = () => {
		setSortProducts(sortProducts === 'asc' ? 'desc' : 'asc');
	};

	return (
		<div className="flex justify-end my-6">
			{sortProducts === 'asc' ? (
				<BiSortDown
					onClick={handleSortToggle}
					className="text-3xl text-blue-950 dark:text-gray-500 cursor-pointer hover:scale-105 transition"
				/>
			) : (
				<BiSortUp
					onClick={handleSortToggle}
					className="text-3xl text-blue-950 dark:text-gray-500 cursor-pointer hover:scale-105 transition"
				/>
			)}
		</div>
	);
};

export default SortArrows;
