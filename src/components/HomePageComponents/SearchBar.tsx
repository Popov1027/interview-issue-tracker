import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

interface SearchBarProps {
  searchInput: string;
  setSearchInput: React.Dispatch<React.SetStateAction<string>>;
}

const SearchBar: FC<SearchBarProps> = ({ searchInput, setSearchInput }) => {
	const { t } = useTranslation();

	return (
		<input
			type="text"
			value={searchInput}
			onChange={(e) => setSearchInput(e.target.value)}
			placeholder={t('homePage.searchProducts')}
			className="block border border-gray-300 dark:border-gray-600 dark:bg-[#1f2937] w-full rounded-2xl appearance-none focus:outline-none py-2 px-4"
		/>
	);
};

export default SearchBar;
