import React, { FC, useContext } from 'react';
import { ProductDto } from '../../api-models/Api';
import moment from 'moment';
import { FaTrashAlt } from 'react-icons/fa';
import { RiEdit2Line } from 'react-icons/ri';
import { useModalStore } from '../../store/useModalStore';
import { useConfirmModal } from '../../store/useConfirmModal';
import { UserDataContext, UserDataContextInterface } from '../../context/UserDataContext';

interface ProductInterface {
  product: ProductDto | undefined;
}

const Product: FC<ProductInterface> = ({ product }) => {
	const { setIsOpenConfirmModal } = useConfirmModal();
	const { role } = useContext<UserDataContextInterface>(UserDataContext);
	const { setIsOpenModal } = useModalStore();

	return (
		<div className="flex justify-center items-center h-96 mt-20">
			<div className="flex flex-col h-full justify-between w-1/2 p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg dark:border-gray-700 peer-checked:border-blue-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 dark:text-gray-400 dark:bg-gray-800">
				<div className="flex justify-between">
					<div className="block space-y-4">
						<p className="w-full text-3xl font-semibold">{product?.name}</p>
						<p className="w-full text-sm">Created at: {moment(product?.createdAt).format('LL')}</p>
						<p className="w-full text-xl">{product?.description}</p>
					</div>
					<p className="text-lg font-light">{product?.brand?.name}</p>
				</div>
				<div className="flex items-center justify-between">
					<div className="flex text-2xl space-x-2">
						<p>
              Price: <span>{product?.price}</span>
						</p>
						<span>{product?.currency}</span>
					</div>
					{role === 'ADMIN' ? (
						<div className="flex space-x-2 items-center">
							<FaTrashAlt
								onClick={() => setIsOpenConfirmModal(true)}
								className="text-2xl cursor-pointer hover:text-red-600 transition"
							/>
							<RiEdit2Line
								onClick={() => setIsOpenModal(true)}
								className="text-3xl cursor-pointer hover:text-green-600 transition"
							/>
						</div>
					) : null}
				</div>
			</div>
		</div>
	);
};

export default Product;
