import React, { FC } from 'react';
import { useModalStore } from '../../store/useModalStore';
import { BrandDto, CreateProductDto, CustomError, ProductDto } from '../../api-models/Api';
import { getBrands } from '../../services/BrandService/brandService';
import { editProduct } from '../../services/ProductService/productService';
import { queryClient } from '../../index';
import { toast } from 'react-toastify';
import { useCreateProductSchema } from '../../hooks/createProductSchema';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { useMutation, useQuery } from 'react-query';
import ProductForm from '../../utilities/ProductForm';

interface EditProductModalInterface {
  product: ProductDto | undefined;
}

const EditProductModal: FC<EditProductModalInterface> = ({ product }) => {
	const { setIsOpenModal } = useModalStore();
	const { data: brands } = useQuery<BrandDto[], Error>('brands', getBrands);

	const { mutate: newProduct } = useMutation(
		(productData: CreateProductDto) => editProduct(product?.id, productData),
		{
			onSuccess: async () => {
				await queryClient.invalidateQueries('product');
				toast.success('Product has been edited');
				setIsOpenModal(false);
			},
			onError: (error: CustomError) => {
				if (error.response.status === 400) {
					toast.error('Invalid credentials');
				}
			}
		}
	);

	const createProductSchema = useCreateProductSchema();

	const methods = useForm<CreateProductDto>({
		resolver: zodResolver(createProductSchema)
	});

	const { handleSubmit } = methods;

	return (
		<>
			<ProductForm
				methods={methods}
				brands={brands}
				handleSubmit={handleSubmit}
				newProduct={newProduct}
				product={product}
			/>
		</>
	);
};

export default EditProductModal;
