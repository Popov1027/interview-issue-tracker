import React, {FC} from 'react';
import {useTranslation} from 'react-i18next';
import {UserDto} from '../../api-models/Api';
import {UseMutateFunction} from 'react-query';
import {MdDelete} from 'react-icons/md';
import ExportFileButton from '../../utilities/ExportFileButton';

interface UsersTableInterface {
    users: UserDto[] | undefined;
	deleteMutation: UseMutateFunction<void, Error, string | undefined>
}

const UsersTable: FC<UsersTableInterface> = ({users, deleteMutation}) => {
	const {t} = useTranslation();
	return (
		<div className="relative overflow-x-auto shadow-md sm:rounded-lg mx-2 tablet:mx-16 mt-12 space-y-4">
			<div className="flex justify-end">
				<ExportFileButton data={users}/>
			</div>
			<table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
				<thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
					<tr>
						<th scope="col" className="px-6 py-3">
							{t('usersPage.firstName')}
						</th>
						<th scope="col" className="px-6 py-3">
							{t('usersPage.lastName')}
						</th>
						<th scope="col" className="px-6 py-3">
							{t('usersPage.email')}
						</th>
						<th scope="col" className="px-6 py-3">
							{t('usersPage.gender')}
						</th>
						<th scope="col" className="px-6 py-3">
							{t('usersPage.role')}
						</th>
						<th scope="col" className="px-6 py-3">
							{t('usersPage.phoneNumber')}
						</th>
						<th scope="col" className="px-6 py-3">
						</th>
					</tr>
				</thead>
				<tbody>
					{users?.map((user) => (
						<tr
							key={user.id}
							className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
							<th scope="row"
								className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
								{user.firstName}
							</th>
							<td className="px-6 py-4">
								{user.lastName}
							</td>
							<td className="px-6 py-4">
								{user.email}
							</td>
							<td className="px-6 py-4">
								{user.gender !== null ? user.gender : '-'}
							</td>
							<td className="px-6 py-4">
								{user.role !== null ? user.role : 'USER'}
							</td>
							<td className="px-6 py-4">
								{user.phoneNumber}
							</td>
							<td className="px-6 py-4">
								<MdDelete
									className="text-xl transition cursor-pointer hover:text-red-600"
									onClick={() => deleteMutation((user.id).toString())}/>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>

	);
};

export default UsersTable;