import React from 'react';
import {useMutation} from 'react-query';
import {CreateBrandDto, CustomError} from '../../api-models/Api';
import {queryClient} from '../../index';
import {toast} from 'react-toastify';
import {createBrand} from '../../services/BrandService/brandService';
import {useBrandModal} from '../../store/useBrandModal';
import {useForm} from 'react-hook-form';
import {zodResolver} from '@hookform/resolvers/zod';
import BrandForm from '../../utilities/BrandForm';
import {useCreateBrandSchema} from '../../hooks/createBrandSchema';

const CreateBrandModal = () => {
	const {isOpenBrandModal, setIsOpenBrandModal} = useBrandModal();

	const {mutate: brand} = useMutation(
		(brandData: CreateBrandDto) => createBrand(brandData),
		{
			onSuccess: async () => {
				await queryClient.invalidateQueries('brands');
				toast.success('Brand has been created');
				setIsOpenBrandModal(false);
			},
			onError: (error: CustomError) => {
				if (error.response.status === 400) {
					toast.error(error.response.message);
				}
			}
		}
	);

	const createBrandSchema = useCreateBrandSchema();

	const methods = useForm<CreateBrandDto>({
		resolver: zodResolver(createBrandSchema)
	});

	const {handleSubmit} = methods;


	return (
		<>
			{isOpenBrandModal ?
				<BrandForm
					methods={methods}
					handleSubmit={handleSubmit}
					brand={brand}
				/> :
				null
			}
		</>

	);
};

export default CreateBrandModal;