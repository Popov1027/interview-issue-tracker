import React, {FC} from 'react';
import {useMutation} from 'react-query';
import {BrandDto, CreateBrandDto, CustomError} from '../../api-models/Api';
import {editBrand} from '../../services/BrandService/brandService';
import {queryClient} from '../../index';
import {toast} from 'react-toastify';
import {useCreateBrandSchema} from '../../hooks/createBrandSchema';
import {useForm} from 'react-hook-form';
import {zodResolver} from '@hookform/resolvers/zod';
import BrandForm from '../../utilities/BrandForm';
import {useBrandModal} from '../../store/useBrandModal';

interface EditBrandModalInterface {
    brandData: BrandDto | undefined;
}

const EditBrandModal:FC<EditBrandModalInterface> = ({brandData}) => {
	const {isOpenBrandModal, setIsOpenBrandModal} = useBrandModal();

	const {mutate: brand} = useMutation(
		(newBrand: CreateBrandDto) => editBrand(newBrand, brandData?.id),
		{
			onSuccess: async () => {
				await queryClient.invalidateQueries('brand');
				toast.success('Brand has been edited');
				setIsOpenBrandModal(false);
			},
			onError: (error: CustomError) => {
				if (error.response.status === 400) {
					toast.error(error.response.message);
				}
			}
		}
	);

	const createBrandSchema = useCreateBrandSchema();

	const methods = useForm<CreateBrandDto>({
		resolver: zodResolver(createBrandSchema),
		defaultValues: {
			name: brandData?.name || '',
		},
	});

	const {handleSubmit} = methods;

	return (
		<>
			{
				isOpenBrandModal ?
					<BrandForm
						methods={methods}
						handleSubmit={handleSubmit}
						brand={brand}
						brandData={brandData}
					/>
					: null
			}
		</>
	);
};

export default EditBrandModal;