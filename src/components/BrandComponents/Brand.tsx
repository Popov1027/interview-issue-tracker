import React, {FC} from 'react';
import {FaTrashAlt} from 'react-icons/fa';
import {RiEdit2Line} from 'react-icons/ri';
import {BrandDto} from '../../api-models/Api';
import moment from 'moment';
import {useNavigate} from 'react-router-dom';
import {UseMutateFunction} from 'react-query';
import {useBrandModal} from '../../store/useBrandModal';
import EditBrandModal from './EditBrandModal';
import {useTranslation} from 'react-i18next';

interface BrandInterface {
    brand: BrandDto | undefined;
    deleteMutation: UseMutateFunction<void, Error, string | undefined, unknown>
}

const Brand: FC<BrandInterface> = ({brand, deleteMutation}) => {
	const {setIsOpenBrandModal} = useBrandModal();
	const { t } = useTranslation();
	const navigate = useNavigate();

	return (
		<div className="flex justify-center items-center h-96 mt-20">
			<div
				className="flex flex-col h-full justify-between w-1/2 p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg dark:border-gray-700 peer-checked:border-blue-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 dark:text-gray-400 dark:bg-gray-800">
				<div className="flex flex-col">
					<div className="block space-y-4">
						<p className="w-full text-3xl font-semibold">{brand?.name}</p>
						<p className="w-full text-sm">{moment(brand?.createdAt).format('lll')}</p>
					</div>
					<div className="flex space-x-2 mt-12">
						{brand?.products?.map((product) => (
							<p
								key={product.id}
								onClick={() => navigate(`/product/${product.id}`)}
								className="flex items-center space-x-2 cursor-pointer transition duration-300 hover:bg-gray-800 hover:text-white dark:hover:bg-white dark:hover:text-blue-950 rounded-full font-bold border py-1 px-3 h-10"
							>
								{product.name}
							</p>
						))}
					</div>
				</div>
				<div className="flex items-center justify-between">
					<div className="flex text-2xl space-x-2">
						{brand?.createdAt !== brand?.updatedAt &&
                            <p>
                            	{t('brand.updatedAt')} <span>{moment(brand?.updatedAt).format('ll')}</span>
                            </p>
						}
					</div>
					<div className="flex space-x-2 items-center">
						<FaTrashAlt
							onClick={() => deleteMutation(brand?.id)}
							className="text-2xl cursor-pointer hover:text-red-600 transition"
						/>
						<RiEdit2Line
							onClick={() => setIsOpenBrandModal(true)}
							className="text-3xl cursor-pointer hover:text-green-600 transition"
						/>
						<EditBrandModal brandData={brand}/>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Brand;