import React from 'react';
import {FaPlus} from 'react-icons/fa';
import {useBrandModal} from '../../store/useBrandModal';
import {useTranslation} from 'react-i18next';

const CreateBrandButton = () => {
	const { t } = useTranslation();
	const {setIsOpenBrandModal} = useBrandModal();

	return (
		<div
			onClick={() => setIsOpenBrandModal(true)}
			className="flex items-center justify-center space-x-2 w-44 cursor-pointer transition duration-300 hover:bg-gray-800 hover:text-white dark:hover:bg-white dark:hover:text-blue-950 rounded-full font-bold border py-1 px-3 h-10"
		>
			<FaPlus />
			<p>{t('homePage.createBrand')}</p>
		</div>
	);
};

export default CreateBrandButton;