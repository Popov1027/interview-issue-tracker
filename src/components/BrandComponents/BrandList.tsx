import React, {FC, useContext, useState} from 'react';
import {BrandDto} from '../../api-models/Api';
import {MdModeEdit} from 'react-icons/md';
import {UserDataContext, UserDataContextInterface} from '../../context/UserDataContext';
import {useNavigate} from 'react-router-dom';

interface BrandList {
    brands: BrandDto[] | undefined;
    setSelectedCategory: React.Dispatch<React.SetStateAction<number | null>>;
    selectedCategory: number | null;
}

const BrandList: FC<BrandList> = ({
	brands,
	setSelectedCategory,
	selectedCategory,
}) => {
	const { role } = useContext<UserDataContextInterface>(UserDataContext);
	const [hoverCategoryId, setHoverCategoryId] = useState<string>('');
	const navigate = useNavigate();

	return (
		<div className="z-10 rounded-lg shadow w-full laptop:w-60 h-[440px] space-y-4">
			{brands?.map((brand) => (
				<div
					key={brand.id}
					onMouseEnter={() => setHoverCategoryId(brand.id)}
					onMouseLeave={() => setHoverCategoryId('')}
					onClick={() => Number(brand.id) === selectedCategory ? setSelectedCategory(null) : setSelectedCategory(Number(brand.id))}
					className={`w-full laptop:w-52 flex items-center justify-between rounded-full px-4 py-2 text-white border cursor-pointer relative ${
						Number(brand.id) === selectedCategory ? 'bg-blue-500 font-bold' : 'bg-gray-800 hover:bg-blue-500 transition duration-500'
					}`}
				>
					<p>{brand.name}</p>
					<>
						{
							role === 'ADMIN' &&
							<div
								className={`absolute right-3 ${hoverCategoryId === brand.id ? 'flex' : 'hidden'} text-xl space-x-1`}>
								<MdModeEdit
									onClick={(e) => {
										e.stopPropagation();
										navigate(`/brand/${brand.id}`);
									}}
									className="hover:text-green-700 transition duration-400"
								/>
							</div>
						}
					</>
				</div>
			))}
		</div>
	);
};

export default BrandList;