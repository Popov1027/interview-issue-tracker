import { Dispatch, FC, SetStateAction, useEffect } from 'react';
import { BehaviorSubject, map } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

interface ChangeThemeContainerInterface {
  setNewThemeSubject: Dispatch<SetStateAction<BehaviorSubject<string> | undefined>>;
}

const ChangeThemeContainer: FC<ChangeThemeContainerInterface> = ({ setNewThemeSubject }) => {
	const themeSubject = new BehaviorSubject('light');

	useEffect(() => {
		const savedTheme = localStorage.getItem('theme');
		const initialTheme = savedTheme || 'light';
		themeSubject.next(initialTheme);

		const subscription = themeSubject
			.pipe(
				distinctUntilChanged(),
				map((selectedTheme) => {
					document.documentElement.classList.remove('dark');
					if (selectedTheme === 'dark') {
						document.documentElement.classList.add('dark');
					}
					localStorage.setItem('theme', selectedTheme);
					return selectedTheme;
				})
			)
			.subscribe();

		setNewThemeSubject(themeSubject);

		return () => {
			subscription.unsubscribe();
		};
	}, [setNewThemeSubject]);

	return <></>;
};

export default ChangeThemeContainer;
