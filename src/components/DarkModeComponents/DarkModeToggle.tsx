import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

interface DarkModeToggleInterface {
  toggleTheme: () => void;
}

const DarkModeToggle: FC<DarkModeToggleInterface> = ({ toggleTheme }) => {
	const { t } = useTranslation();
	const theme = localStorage.getItem('theme');

	return (
		<div className="flex items-center justify-center space-x-2">
			<h1 className="font-bold">{t('settingsPage.theme')}:</h1>
			<select
				onChange={() => toggleTheme()}
				defaultValue={theme || 'dark'}
				className="cursor-pointer block appearance-none border border-gray-300 rounded px-4 py-2 dark:text-white dark:bg-[#1F2937] dark:border-gray-900 focus:outline-none focus:border-blue-500"
			>
				<option value="dark">{t('settingsPage.dark')}</option>
				<option value="light">{t('settingsPage.light')}</option>
			</select>
		</div>
	);
};

export default DarkModeToggle;
