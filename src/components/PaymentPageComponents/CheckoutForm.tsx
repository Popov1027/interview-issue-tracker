import {
	useStripe,
	useElements,
	CardNumberElement,
	CardExpiryElement,
	CardCvcElement
} from '@stripe/react-stripe-js';
import {FormEvent} from 'react';
import {useTranslation} from 'react-i18next';

const PaymentForm = () => {
	const { t } = useTranslation();
	const stripe = useStripe();
	const elements = useElements();

	const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		const { error, paymentMethod } = await stripe!.createPaymentMethod({
			type: 'card',
			card: elements!.getElement(CardNumberElement)!
		});

		if (!error) {
			console.log(paymentMethod);
		}
	};

	return (
		<div className="flex justify-center items-center h-screen">
			<form onSubmit={handleSubmit} className="w-full max-w-2xl border p-8 rounded-lg shadow-lg">
				<div className="flex justify-center items-center text-2xl font-bold">
					<h1>{t('payment.paymentDetails')}</h1>
				</div>
				<div className="mb-4">
					<label htmlFor="cardNumber" className="block mb-2 font-bold">{t('payment.cardNumber')}</label>
					<CardNumberElement id="cardNumber" className="w-full p-2 rounded border border-gray-300 py-4 focus:border-blue-500 focus:outline-none" />
				</div>
				<div className="mb-4">
					<label htmlFor="expiry" className="block mb-2 font-bold">{t('payment.expirationDate')}</label>
					<CardExpiryElement id="expiry" className="w-full p-2 rounded border border-gray-300 py-4 focus:border-blue-500 focus:outline-none" />
				</div>
				<div className="mb-4">
					<label htmlFor="cvc" className="block mb-2 font-bold">{t('payment.cvc')}</label>
					<CardCvcElement id="cvc" className="w-full p-2 rounded border border-gray-300 py-4 focus:border-blue-500 focus:outline-none" />
				</div>
				<div className="text-center">
					<button
						className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
						type="submit" disabled={!stripe}>
						{t('payment.pay')}
					</button>
				</div>
			</form>
		</div>
	);
};

export default PaymentForm;
