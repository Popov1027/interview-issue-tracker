import React, { FC } from 'react';
import { ProductDto } from '../../api-models/Api';
import { useTranslation } from 'react-i18next';
import {useNavigate} from 'react-router-dom';
import {useOrderStore} from '../../store/useOrderStore';

interface CartInterface {
  shoppingCart: ProductDto[];
  totalPrice: number;

  handleDeleteProduct(id: string): void;

  handleQuantityChange(id: string, quantity: number): void;

  increaseQuantity(id: string): void;

  decreaseQuantity(id: string): void;

  quantities: { [p: string]: number };
}

const imageMapper = {
	nike: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/de8ccccd-40e8-4bcd-b86e-ab539876dd11/custom-nike-air-force-1-low-by-you.png',
	adidas:
    'https://assets.adidas.com/images/w_600,f_auto,q_auto/bd43ce71f589498ab6b1aad6009a0e6e_9366/Superstar_Shoes_White_EG4958_07_standard.jpg'
};

const Cart: FC<CartInterface> = ({
	shoppingCart,
	totalPrice,
	handleDeleteProduct,
	handleQuantityChange,
	increaseQuantity,
	decreaseQuantity,
	quantities
}) => {
	const { t } = useTranslation();

	const {setIsOrder} = useOrderStore();

	const navigate = useNavigate();
	const productImage = imageMapper['nike'];

	return (
		<div className="h-screen mt-20">
			<h1 className="mb-10 text-center text-3xl font-bold">{t('shoppingCart.cartItems')}</h1>
			<div className="mx-auto max-w-5xl justify-center px-6 flex space-x-6">
				<div className="w-full">
					{shoppingCart.map((cartItem) => (
						<div key={cartItem.id} className="rounded-lg">
							<div className="justify-between mb-6 rounded-lg p-6 shadow-xl border border-gray-700 sm:flex sm:justify-start">
								<img src={productImage} alt="product-image" className="w-full rounded-lg sm:w-40" />
								<div className="sm:ml-4 sm:flex sm:w-full sm:justify-between">
									<div className="mt-5 sm:mt-0">
										<h2 className="text-lg font-bold">{cartItem.name}</h2>
									</div>
									<div className="mt-4 flex justify-between sm:space-y-6 sm:mt-0 sm:block sm:space-x-6">
										<div className="flex items-center border-gray-100">
											<span
												onClick={() => decreaseQuantity(cartItem.id)}
												className="cursor-pointer rounded-l bg-gray-700 py-1 px-3.5 duration-100 hover:bg-blue-500 hover:text-blue-50"
											>
                        -
											</span>
											<input
												className="h-8 w-8 bg-gray-500 text-center text-xs outline-none"
												type="number"
												value={quantities[cartItem.id]}
												onChange={(e) =>
													handleQuantityChange(cartItem.id, parseInt(e.target.value, 10))
												}
												min="1"
											/>
											<span
												onClick={() => increaseQuantity(cartItem.id)}
												className="cursor-pointer rounded-r bg-gray-700 py-1 px-3 duration-100 hover:bg-blue-500 hover:text-blue-50"
											>
                        +
											</span>
										</div>
										<div className="flex items-center space-x-4">
											<p className="text-sm">
												{cartItem.price} {cartItem.currency}
											</p>
											<svg
												onClick={() => handleDeleteProduct(cartItem.id)}
												xmlns="http://www.w3.org/2000/svg"
												fill="none"
												viewBox="0 0 24 24"
												strokeWidth="1.5"
												stroke="currentColor"
												className="h-5 w-5 cursor-pointer duration-150 hover:text-red-500"
											>
												<path
													strokeLinecap="round"
													strokeLinejoin="round"
													d="M6 18L18 6M6 6l12 12"
												/>
											</svg>
										</div>
									</div>
								</div>
							</div>
						</div>
					))}
				</div>
				<div className="mt-6 h-[156px] rounded-lg border border-gray-700 p-6 shadow-md md:mt-0 md:w-1/3">
					<div className="flex justify-between">
						<p className="text-lg font-bold">Total</p>
						<p className="mb-1 text-lg font-bold">{totalPrice.toFixed(0)}$</p>
					</div>
					<button
						onClick={() => {
							setIsOrder(true);
							localStorage.setItem('total_price', totalPrice.toString());
							navigate('/payment');
						}}
						className="mt-8 w-full rounded-md bg-blue-500 py-1.5 font-medium text-blue-50 hover:bg-blue-600">
						{t('shoppingCart.order')}
					</button>
				</div>
			</div>
		</div>
	);
};

export default Cart;
