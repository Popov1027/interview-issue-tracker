import React, {useContext} from 'react';
import {PiDogFill} from 'react-icons/pi';
import {RxHamburgerMenu} from 'react-icons/rx';
import {Link} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import {IoCloseSharp} from 'react-icons/io5';
import {UserDataContext, UserDataContextInterface} from '../../context/UserDataContext';
import {useNavBarMenu} from '../../store/useNavBarMenu';
import {useNavBarLinks} from '../../hooks/useNavBarLinks';

const NavBar: React.FC = () => {
	const {t} = useTranslation();
	const {isOpenMenu, setIsOpenMenu} = useNavBarMenu();
	const {role} = useContext<UserDataContextInterface>(UserDataContext);
	const links = useNavBarLinks();

	return (
		<div className="border border-gray-600 rounded-full m-3">
			<nav className="relative px-4 py-4 flex justify-between items-center">
				<Link
					to={'/'}
					className="font-bold leading-none flex items-center space-x-2 m-0 tablet:ml-12"
				>
					<PiDogFill className="text-5xl"/>
					<p className="text-3xl">Inuarashi</p>
					{role === 'ADMIN' ? (
						<span className="uppercase rounded-full px-2 text-sm border-2">admin</span>
					) : null}
				</Link>
				<div className="lg:hidden">
					<button
						className="navbar-burger flex items-center text-blue-600 p-3"
						onClick={() => setIsOpenMenu(!isOpenMenu)}
					>
						<RxHamburgerMenu/>
					</button>
				</div>
				<div
					className="hidden absolute top-1/2 right-0 transform -translate-y-1/2 -translate-x-1/2 lg:flex lg:mx-auto lg:items-center lg:w-auto lg:space-x-6">
					{links.map((link, index) => (
						<Link
							key={index}
							to={link.path}
							onClick={() => setIsOpenMenu(false)}
							className="font-bold"
						>
							{link.title}
						</Link>
					))}
					{role === 'ADMIN' && <Link className="font-bold" to={'/users'}>Users</Link>}
					<Link
						className="font-bold border rounded-full px-4 py-2 hover:bg-blue-500 transition hover:text-white"
						to={'/sign-in'}
						onClick={() => localStorage.removeItem('access_token')}
					>
						{t('navBar.logout')}
					</Link>
				</div>
			</nav>
			<div className={`navbar-menu relative z-50 ${isOpenMenu ? '' : 'hidden'}`}>
				<div className="navbar-backdrop fixed inset-0 bg-gray-800 opacity-70"
					onClick={() => setIsOpenMenu(false)}></div>
				<nav
					className={`fixed top-0 left-0 bottom-0 flex flex-col w-5/6 max-w-sm py-6 px-6 bg-white dark:bg-[#1F2937] border-r overflow-y-auto transition-transform duration-300 ${
						isOpenMenu ? 'translate-x-0' : '-translate-x-full'
					}`}
				>
					<div className="flex items-center mb-8 relative">
						<Link to={'/'} className="font-bold leading-none flex items-center space-x-2">
							<PiDogFill className="text-5xl"/>
							<p className="text-3xl">Inuarashi</p>
						</Link>
						<button className="navbar-close" onClick={() => setIsOpenMenu(false)}>
							<IoCloseSharp className="text-3xl absolute top-0 right-0"/>
						</button>
					</div>
					<div className="mt-4 h-full flex flex-col justify-between gap-4 relative">
						<div className="flex flex-col space-y-4">
							{links.map((link, index) => (
								<Link
									key={index}
									to={link.path}
									onClick={() => setIsOpenMenu(false)}
									className="font-bold"
								>
									{link.title}
								</Link>
							))}
							{role === 'ADMIN' && <Link className="font-bold" to={'/users'}>Users</Link>}
						</div>
						<Link
							className="font-bold text-center border rounded-full px-4 py-2 mb-12 hover:bg-blue-500 transition hover:text-white"
							to={'/sign-in'}
							onClick={() => {
								localStorage.clear();
							}}
						>
							{t('navBar.logout')}
						</Link>
					</div>
				</nav>
			</div>
		</div>
	);
};

export default NavBar;
