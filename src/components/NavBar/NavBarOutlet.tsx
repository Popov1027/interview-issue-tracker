import NavBar from './NavBar';
import { Outlet } from 'react-router-dom';

const NavBarOutlet = (): JSX.Element => {
	return (
		<>
			<NavBar />
			<Outlet />
		</>
	);
};

export default NavBarOutlet;
