import React from 'react';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';

const TranslationButton = () => {
	const { i18n, t } = useTranslation();
	const storedLng = localStorage.getItem('lng');

	const changeLanguage = (lng: string) => {
		i18n.changeLanguage(lng).then(() => {
			toast.success(t('settingsPage.changedLanguage'));
		});
		localStorage.setItem('lng', lng);
	};

	return (
		<div className="flex items-center justify-center space-x-2">
			<h1 className="font-bold">{t('settingsPage.language')}:</h1>
			<select
				onChange={(e) => changeLanguage(e.target.value)}
				defaultValue={storedLng || 'en'}
				className="cursor-pointer block appearance-none border border-gray-300 rounded px-4 py-2 dark:text-white dark:bg-[#1F2937] dark:border-gray-900 focus:outline-none focus:border-blue-500"
			>
				<option value="en">{t('settingsPage.english')}</option>
				<option value="ro">{t('settingsPage.romanian')}</option>
			</select>
		</div>
	);
};

export default TranslationButton;
