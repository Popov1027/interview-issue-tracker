import { createContext } from 'react';

export interface UserDataContextInterface {
  role: string | null | undefined;
}

export const UserDataContext = createContext<UserDataContextInterface>({
	role: undefined
});
