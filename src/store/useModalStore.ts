import create from 'zustand';

interface ModalStore {
  isOpenModal: boolean;
  setIsOpenModal: (isOpen: boolean) => void;
}

export const useModalStore = create<ModalStore>((set) => ({
	isOpenModal: false,
	setIsOpenModal: (isOpen: boolean) => set({ isOpenModal: isOpen })
}));
