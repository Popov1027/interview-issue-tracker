import create from 'zustand';

interface OpenBookTypeInterface {
    isOpenBookType: boolean;
    setIsOpenBookType: (isOpen: boolean) => void;
}

export const useOpenBookType = create<OpenBookTypeInterface>((set) => ({
	isOpenBookType: false,
	setIsOpenBookType: (isOpen: boolean) => set({ isOpenBookType: isOpen })
}));
