import create from 'zustand';

interface BookStore {
    bookType: 'xlsx' | 'csv';
    setBookType: (newType: 'xlsx' | 'csv') => void;
}

export const useBookStore = create<BookStore>((set) => ({
	bookType: 'xlsx',
	setBookType: (newType) => set({ bookType: newType }),
}));