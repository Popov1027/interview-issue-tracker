import create from 'zustand';

interface ModalStore {
  isOpenConfirmModal: boolean;
  setIsOpenConfirmModal: (isOpen: boolean) => void;
}

export const useConfirmModal = create<ModalStore>((set) => ({
	isOpenConfirmModal: false,
	setIsOpenConfirmModal: (isOpen: boolean) => set({ isOpenConfirmModal: isOpen })
}));
