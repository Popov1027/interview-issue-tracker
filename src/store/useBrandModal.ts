import create from 'zustand';

interface BrandModalInterface {
    isOpenBrandModal: boolean;
    setIsOpenBrandModal: (isOpen: boolean) => void;
}

export const useBrandModal = create<BrandModalInterface>((set) => ({
	isOpenBrandModal: false,
	setIsOpenBrandModal: (isOpen: boolean) => set({ isOpenBrandModal: isOpen })
}));
