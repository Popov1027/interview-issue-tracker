import create from 'zustand';

interface OrderStore {
    isOrder: boolean;
    setIsOrder: (isOpen: boolean) => void;
}

export const useOrderStore = create<OrderStore>((set) => ({
	isOrder: false,
	setIsOrder: (isOrder: boolean) => set({ isOrder: isOrder })
}));
