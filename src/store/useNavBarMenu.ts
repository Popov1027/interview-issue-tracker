import create from 'zustand';

interface NavBarMenuStore {
    isOpenMenu: boolean;
    setIsOpenMenu: (isOpen: boolean) => void;
}

export const useNavBarMenu = create<NavBarMenuStore>((set) => ({
	isOpenMenu: false,
	setIsOpenMenu: (isOpen: boolean) => set({ isOpenMenu: isOpen })
}));
