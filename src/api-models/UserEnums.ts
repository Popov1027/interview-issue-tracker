export enum Role {
  ADMIN = 'ADMIN',
  USER = 'USER'
}

export enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHER = 'OTHER'
}

export enum BookType {
  XLSX = 'xlsx',
  CSV = 'csv'
}