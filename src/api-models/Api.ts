/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CustomError {
  response: {
    status: number;
    message: string;
  };
}

export interface RegisterDto {
  /** @example "user@email.com" */
  email: string;
  /** @example "Denis" */
  firstName: string;
  /** @example "R." */
  lastName: string;
  /** @example "+37379062693" */
  phoneNumber: string;
  /** @example "PassC@d3" */
  password: string;
}

export interface TokensDto {
  accessToken: string;
}

export interface LoginDto {
  /** @example "user@email.com" */
  email: string;
  /** @example "PassC@d3" */
  password: string;
}

export interface CreateUserDto {
  /** @example "Denis" */
  firstName: string;
  /** @example "Rotari" */
  lastName: string;
  /** @example "MALE" */
  gender?: 'MALE' | 'FEMALE' | 'OTHER' | null;
  /** @example "user@email.com" */
  email: string;
  /** @example "P@ssword12" */
  password: string;
  /** @example "88822212" */
  phoneNumber: string;
}

export interface UserDto {
  /** @example "121" */
  id: number;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  createdAt: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  updatedAt: string;
  /** @example "Denis" */
  firstName: string;
  /** @example "Rotari" */
  lastName: string;
  /** @example "MALE" */
  gender?: 'MALE' | 'FEMALE' | 'OTHER' | null;
  /**
   * @default "USER"
   * @example "USER"
   */
  role?: 'ADMIN' | 'USER ' | null;
  /** @example "user@email.com" */
  email: string;
  /** @example "88822212" */
  phoneNumber: string;
}

export interface UpdateUserDto {
  /** @example "Denis" */
  firstName?: string;
  /** @example "Rotari" */
  lastName?: string;
  /** @example "MALE" */
  gender?: 'MALE' | 'FEMALE' | 'OTHER' | null;

  role?: 'ADMIN' | 'USER ' | null;
  /** @example "user@email.com" */
  email?: string;
  /** @example "P@ssword12" */
  password?: string;
  /** @example "88822212" */
  phoneNumber?: string;
}

export interface CreateProductDto {
  /** @example "Nike Jordan" */
  name: string;
  description?: string | null;
  currency: string;
  price: number;
  /** @example 6 */
  brandId: number;
}

export interface ProductDto {
  /** @example "121" */
  id: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  createdAt: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  updatedAt: string;
  /** @example "Nike Jordan" */
  name: string;
  description?: string | null;
  currency: string;
  price: number;
  /** @example 6 */
  brandId?: number;
  brand?: BrandDto;
  quantity?: number;
}

export interface UpdatedProductDto {
  /** @example "121" */
  id: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  createdAt: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  updatedAt: string;
  /** @example "Nike Jordan" */
  name: string;
  description?: string | null;
  currency: string;
  price: number;
  /** @example 6 */
  brandId?: number;
  brand?: string;
  quantity?: number;
}

export interface BrandDto {
  /** @example "121" */
  id: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  createdAt: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  updatedAt: string;
  /** @example "Adidas" */
  name: string;
  products?: ProductDto[];
}

export interface UpdateProductDto {
  /** @example "Nike Jordan" */
  name?: string;
  description?: string | null;
  currency?: string;
  price?: number;
  /** @example 6 */
  brandId?: number;
}

export interface CreateAppointmentDto {
  /** @example "Doctor appointment" */
  name: string;
  /** @example "15/11/2023" */
  startDate: string;
  /** @example "16/11/2023" */
  endDate: string;
  /** @example 6 */
  userId: number;
}

export interface AppointmentDto {
  /** @example "121" */
  id: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  createdAt: string;
  /**
   * @format date-time
   * @example "2022-01-22T13:55:37.839Z"
   */
  updatedAt: string;
  /** @example "Doctor Appointment" */
  name: string;
  /** @example "15/11/2023" */
  startDate: string;
  /** @example "16/11/2023" */
  endDate: string;
  /** @example 6 */
  userId?: number;
  user?: UserDto;
}

export interface UpdateAppointmentDto {
  /** @example "Doctor appointment" */
  name?: string;
  /** @example "15/11/2023" */
  startDate?: string;
  /** @example "16/11/2023" */
  endDate?: string;
  /** @example 6 */
  userId?: number;
}

export interface CreateBrandDto {
  /** @example "Adidas" */
  name: string;
}

export interface UpdateBrandDto {
  /** @example "Adidas" */
  name?: string;
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, 'body' | 'bodyUsed'>;

export interface FullRequestParams extends Omit<RequestInit, 'body'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, 'baseUrl' | 'cancelToken' | 'signal'>;
  securityWorker?: (
    securityData: SecurityDataType | null
  ) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
  Text = 'text/plain'
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = '';
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: 'same-origin',
    headers: {},
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === 'number' ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join('&');
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => 'undefined' !== typeof query[key]);
    return keys
      .map((key) =>
        Array.isArray(query[key])
          ? this.addArrayQueryParam(query, key)
          : this.addQueryParam(query, key)
      )
      .join('&');
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : '';
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === 'object' || typeof input === 'string')
        ? JSON.stringify(input)
        : input,
    [ContentType.Text]: (input: any) =>
      input !== null && typeof input !== 'string' ? JSON.stringify(input) : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === 'object' && property !== null
              ? JSON.stringify(property)
              : `${property}`
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input)
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {})
      }
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(
      `${baseUrl || this.baseUrl || ''}${path}${queryString ? `?${queryString}` : ''}`,
      {
        ...requestParams,
        headers: {
          ...(requestParams.headers || {}),
          ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {})
        },
        signal: (cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal) || null,
        body: typeof body === 'undefined' || body === null ? null : payloadFormatter(body)
      }
    ).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Interview backend
 * @version 1.0
 * @contact
 *
 * The interview application
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  api = {
    /**
     * No description
     *
     * @name HealthControllerHealthCheck
     * @request GET:/api/health
     */
    healthControllerHealthCheck: (params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/api/health`,
        method: 'GET',
        ...params
      }),

    /**
     * No description
     *
     * @tags Profile
     * @name AuthControllerRegister
     * @summary Register
     * @request POST:/api/register
     */
    authControllerRegister: (data: RegisterDto, params: RequestParams = {}) =>
      this.request<TokensDto, void>({
        path: `/api/register`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Profile
     * @name AuthControllerLogin
     * @summary Login
     * @request POST:/api/login
     */
    authControllerLogin: (data: LoginDto, params: RequestParams = {}) =>
      this.request<TokensDto, void>({
        path: `/api/login`,
        method: 'POST',
        body: data,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags users
     * @name UserControllerCreate
     * @summary Endpoint to create user
     * @request POST:/api/user
     * @secure
     */
    userControllerCreate: (data: CreateUserDto, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/api/user`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags users
     * @name UserControllerFindAll
     * @summary Endpoint to get all users
     * @request GET:/api/user
     * @secure
     */
    userControllerFindAll: (params: RequestParams = {}) =>
      this.request<UserDto[], void>({
        path: `/api/user`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags users
     * @name UserControllerFindOne
     * @summary Endpoint to get user
     * @request GET:/api/user/{id}
     * @secure
     */
    userControllerFindOne: (id: string, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/api/user/${id}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags users
     * @name UserControllerUpdate
     * @summary Endpoint to update user
     * @request PATCH:/api/user/{id}
     * @secure
     */
    userControllerUpdate: (id: string, data: UpdateUserDto, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/api/user/${id}`,
        method: 'PATCH',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags users
     * @name UserControllerRemove
     * @summary Endpoint to delete user
     * @request DELETE:/api/user/{id}
     * @secure
     */
    userControllerRemove: (id: string, params: RequestParams = {}) =>
      this.request<UserDto, void>({
        path: `/api/user/${id}`,
        method: 'DELETE',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Product
     * @name ProductControllerCreate
     * @summary Endpoint to create Product
     * @request POST:/api/product
     * @secure
     */
    productControllerCreate: (data: CreateProductDto, params: RequestParams = {}) =>
      this.request<ProductDto, void>({
        path: `/api/product`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Product
     * @name ProductControllerFindAll
     * @summary Endpoint to get all product
     * @request GET:/api/product
     * @secure
     */
    productControllerFindAll: (params: RequestParams = {}) =>
      this.request<ProductDto[], void>({
        path: `/api/product`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Product
     * @name ProductControllerFindOne
     * @summary Endpoint to get one product
     * @request GET:/api/product/{id}
     * @secure
     */
    productControllerFindOne: (id: string, params: RequestParams = {}) =>
      this.request<ProductDto, void>({
        path: `/api/product/${id}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Product
     * @name ProductControllerUpdate
     * @summary Endpoint to update product
     * @request PATCH:/api/product/{id}
     * @secure
     */
    productControllerUpdate: (id: string, data: UpdateProductDto, params: RequestParams = {}) =>
      this.request<ProductDto, void>({
        path: `/api/product/${id}`,
        method: 'PATCH',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Product
     * @name ProductControllerRemove
     * @summary Endpoint to delete Product
     * @request DELETE:/api/product/{id}
     * @secure
     */
    productControllerRemove: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/product/${id}`,
        method: 'DELETE',
        secure: true,
        ...params
      }),

    /**
     * No description
     *
     * @tags Appointment
     * @name AppointmentControllerCreate
     * @summary Endpoint to create Appointment
     * @request POST:/api/appointment
     * @secure
     */
    appointmentControllerCreate: (data: CreateAppointmentDto, params: RequestParams = {}) =>
      this.request<AppointmentDto, void>({
        path: `/api/appointment`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Appointment
     * @name AppointmentControllerFindAll
     * @summary Endpoint to get all appointments
     * @request GET:/api/appointment
     * @secure
     */
    appointmentControllerFindAll: (params: RequestParams = {}) =>
      this.request<AppointmentDto[], void>({
        path: `/api/appointment`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Appointment
     * @name AppointmentControllerFindAllByUserId
     * @summary Endpoint to get all appointments
     * @request GET:/api/appointment/{userId}
     * @secure
     */
    appointmentControllerFindAllByUserId: (userId: string, params: RequestParams = {}) =>
      this.request<AppointmentDto[], void>({
        path: `/api/appointment/${userId}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Appointment
     * @name AppointmentControllerFindOne
     * @summary Endpoint to get one appointment
     * @request GET:/api/appointment/{id}
     * @secure
     */
    appointmentControllerFindOne: (id: string, params: RequestParams = {}) =>
      this.request<AppointmentDto, void>({
        path: `/api/appointment/${id}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Appointment
     * @name AppointmentControllerUpdate
     * @summary Endpoint to update appointment
     * @request PATCH:/api/appointment/{id}
     * @secure
     */
    appointmentControllerUpdate: (
      id: string,
      data: UpdateAppointmentDto,
      params: RequestParams = {}
    ) =>
      this.request<AppointmentDto, void>({
        path: `/api/appointment/${id}`,
        method: 'PATCH',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Appointment
     * @name AppointmentControllerRemove
     * @summary Endpoint to delete appointment
     * @request DELETE:/api/appointment/{id}
     * @secure
     */
    appointmentControllerRemove: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/appointment/${id}`,
        method: 'DELETE',
        secure: true,
        ...params
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandControllerCreate
     * @summary Endpoint to create Brand
     * @request POST:/api/brand
     * @secure
     */
    brandControllerCreate: (data: CreateBrandDto, params: RequestParams = {}) =>
      this.request<BrandDto, void>({
        path: `/api/brand`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandControllerFindAll
     * @summary Endpoint to get all brands
     * @request GET:/api/brand
     * @secure
     */
    brandControllerFindAll: (params: RequestParams = {}) =>
      this.request<BrandDto[], void>({
        path: `/api/brand`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandControllerFindOne
     * @summary Endpoint to get one brand
     * @request GET:/api/brand/{id}
     * @secure
     */
    brandControllerFindOne: (id: string, params: RequestParams = {}) =>
      this.request<BrandDto, void>({
        path: `/api/brand/${id}`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandControllerUpdate
     * @summary Endpoint to update brand
     * @request PATCH:/api/brand/{id}
     * @secure
     */
    brandControllerUpdate: (id: string, data: UpdateBrandDto, params: RequestParams = {}) =>
      this.request<BrandDto, void>({
        path: `/api/brand/${id}`,
        method: 'PATCH',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandControllerRemove
     * @summary Endpoint to delete brand
     * @request DELETE:/api/brand/{id}
     * @secure
     */
    brandControllerRemove: (id: string, params: RequestParams = {}) =>
      this.request<void, void>({
        path: `/api/brand/${id}`,
        method: 'DELETE',
        secure: true,
        ...params
      })
  };
}
